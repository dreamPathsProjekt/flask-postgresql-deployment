#!/bin/bash


DOCKER_REGISTRY="$1"
FLASK_APP_VERSION: '0.8'

docker build -t "flask-app":"$FLASK_APP_VERSION" ./flask-app/ && \
docker tag flask-app:"$FLASK_APP_VERSION" $DOCKER_REGISTRY/flask-app:"$FLASK_APP_VERSION" && \
docker tag flask-app:"$FLASK_APP_VERSION" $DOCKER_REGISTRY/flask-app:latest && \
docker push $DOCKER_REGISTRY/flask-app:"$FLASK_APP_VERSION" && \
docker push $DOCKER_REGISTRY/flask-app:latest && \
docker stack deploy -c flask-postgresql.yml flask-postgresql