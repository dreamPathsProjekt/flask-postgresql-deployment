from shortener import app, db, models, views, controllers


if __name__ == "__main__":
    app.run(
        debug=False,
        host='0.0.0.0',
        port=5000,
        use_evalex=False,
        threaded=True
    )