from flask_testing import TestCase
from flask import url_for

import shortener


class A_ValidationTestCase(TestCase):


    def create_app(self):
        shortener.app.testing = True
        return shortener.app


    @classmethod
    def setUpClass(cls):
        shortener.controllers.rollback_db()


    @classmethod
    def tearDownClass(cls):
        shortener.controllers.rollback_db()


    def setUp(self):
        self.client = shortener.app.test_client()


    def tearDown(self):
        pass


    def test_index_route(self):
        response = self.client.get(
            url_for('index', description='Input your url'),
            follow_redirects=True
        )

        self.assert200(response)


    def test_url_not_valid(self):
        url_input = 'asddf'

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            )
        )

        valid = shortener.views.validate_url(url_input)
        self.assert200(response)
        self.assertMessageFlashed('The requested url {} was not valid.'.format(url_input), category='error')
        self.assertFalse(valid, msg='Url {} invalid'.format(url_input))


    def test_url_not_valid_missing_protocol(self):
        url_input = 'techcrunch.com/test'

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            )
        )

        valid = shortener.views.validate_url(url_input)
        self.assert200(response)
        self.assertMessageFlashed('The requested url {} was not valid.'.format(url_input), category='error')
        self.assertFalse(valid, msg='Url {} invalid'.format(url_input))


    def test_url_not_valid_wrong_host(self):
        url_input = 'http://techcrunch/test'

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            )
        )

        valid = shortener.views.validate_url(url_input)
        self.assert200(response)
        self.assertMessageFlashed('The requested url {} was not valid.'.format(url_input), category='error')
        self.assertFalse(valid, msg='Url {} invalid'.format(url_input))


    def test_url_empty(self):
        url_input = 'http://techcrunch.com'

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            )
        )

        valid = shortener.views.validate_url(url_input)
        self.assert200(response)
        self.assertMessageFlashed('Empty url path is not permitted !', category='error')
        self.assertFalse(valid, msg='Url {} invalid'.format(url_input))


    def test_url_empty_path(self):
        url_input = 'http://techcrunch.com/?asdd'

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            )
        )

        valid = shortener.views.validate_url(url_input)
        self.assert200(response)
        self.assertMessageFlashed('Not allowed only special characters as url path.', category='error')
        self.assertFalse(valid, msg='Url {} invalid'.format(url_input))


    def test_url_empty_path_with_slash(self):
        url_input = 'http://techcrunch.com/'

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            )
        )

        valid = shortener.views.validate_url(url_input)
        self.assert200(response)
        self.assertMessageFlashed('Not allowed only special characters as url path.', category='error')
        self.assertFalse(valid, msg='Url {} invalid'.format(url_input))


    def test_url_only_special_characters(self):
        url_input = 'http://techcrunch.com/%#&-_/#%-_?asdfg'

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            )
        )

        valid = shortener.views.validate_url(url_input)
        self.assert200(response)
        self.assertMessageFlashed('Not allowed only special characters as url path.', category='error')
        self.assertFalse(valid, msg='Url {} invalid'.format(url_input))


    def test_url_only_zero(self):
        url_input = 'http://techcrunch.com/0'

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            )
        )

        valid = shortener.views.validate_url(url_input)
        self.assert200(response)
        self.assertMessageFlashed('This implementation does not allow zeroes only as url path.', category='error')
        self.assertFalse(valid, msg='Url {} invalid'.format(url_input))


    def test_url_only_zeroes(self):
        url_input = 'http://techcrunch.com/0000/?'

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            )
        )

        valid = shortener.views.validate_url(url_input)
        self.assert200(response)
        self.assertMessageFlashed('This implementation does not allow zeroes only as url path.', category='error')
        self.assertFalse(valid, msg='Url {} invalid'.format(url_input))


    def test_url_valid_normal(self):
        url_input = 'http://techcrunch.com/test'

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            )
        )

        valid = shortener.views.validate_url(url_input)
        self.assert200(response)
        # Database has no records to speed up tests !
        self.assertMessageFlashed('Ooops! Not generated url. The database is full. Cannot store {}'.format(url_input), category='error')
        self.assertTrue(valid, msg='Url {} valid'.format(url_input))


    def test_url_valid_ip_port(self):
        url_input = 'http://127.0.0.1:5000/test'

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            )
        )

        valid = shortener.views.validate_url(url_input)
        self.assert200(response)
        self.assertMessageFlashed('Ooops! Not generated url. The database is full. Cannot store {}'.format(url_input), category='error')
        self.assertTrue(valid, msg='Url {} valid'.format(url_input))


    def test_url_valid_host_port(self):
        url_input = 'http://localhost:5000/test'

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            )
        )

        valid = shortener.views.validate_url(url_input)
        self.assert200(response)
        self.assertMessageFlashed('Ooops! Not generated url. The database is full. Cannot store {}'.format(url_input), category='error')
        self.assertTrue(valid, msg='Url {} valid'.format(url_input))


    def test_url_valid_base_path(self):
        url_input = 'http://localhost:5000/test/index.html'

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            )
        )

        valid = shortener.views.validate_url(url_input)
        self.assert200(response)
        self.assertMessageFlashed('Ooops! Not generated url. The database is full. Cannot store {}'.format(url_input), category='error')
        self.assertTrue(valid, msg='Url {} valid'.format(url_input))


    def test_url_valid_path_parameters(self):
        url_input = 'http://localhost:5000/test/index.html?user=test&session=123456'

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            )
        )

        valid = shortener.views.validate_url(url_input)
        self.assert200(response)
        self.assertMessageFlashed('Ooops! Not generated url. The database is full. Cannot store {}'.format(url_input), category='error')
        self.assertTrue(valid, msg='Url {} valid'.format(url_input))
