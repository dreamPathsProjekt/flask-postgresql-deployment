from flask_testing import TestCase
from flask import url_for

import shortener



class B_FunctionalTestCase(TestCase):


    def create_app(self):
        shortener.app.testing = True
        return shortener.app


    @classmethod
    def setUpClass(cls):
        shortener.controllers.rollback_db()
        result = shortener.controllers.initial_data()
        while result == 'Schema was dropped and recreated. Please run /initdata again.':
            result = shortener.controllers.initial_data()


    @classmethod
    def tearDownClass(cls):
        shortener.controllers.rollback_db()
        result = shortener.controllers.initial_data()
        while result == 'Schema was dropped and recreated. Please run /initdata again.':
            result = shortener.controllers.initial_data()


    def setUp(self):
        self.client = shortener.app.test_client()


    def tearDown(self):
        pass


    def test_index_route(self):
        response = self.client.get(
            url_for('index', description='Input your url'),
            follow_redirects=True
        )

        self.assert200(response)


    def test_url_01_first_time(self):
        url_input = 'http://localhost:5000/test/'

        # Warning key.offset mutates
        key_before = shortener.models.Key.query.get('t')
        key_before_offset = key_before.offset
        word_before = shortener.models.Word.query.get(key_before.offset)

        self.assertEqual(key_before.id, 't')
        self.assertFalse(key_before.occupied)
        self.assertEqual(key_before.offset, 22824)

        self.assertEqual(word_before.id, 22824)
        self.assertEqual(word_before.slug, 't')
        self.assertEqual(word_before.letter, 't')
        self.assertFalse(word_before.occupied)

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            ),
            follow_redirects=True
        )

        self.assert200(response)
        self.assertContext(
            name='description',
            value='Input your url',
        )
        self.assertContext(
            name='url_message',
            value=url_input
        )
        self.assertContext(
            name='short_url',
            value='http://domain.com/t'
        )
        self.assertContext(
            name='status_message',
            value='Url was not found. Stored in database.'
        )

        url_stored = shortener.models.Url.query.get(shortener.controllers.hash_url(url_input))
        key_after = shortener.models.Key.query.get('t')
        word_updated = shortener.models.Word.query.get(key_before_offset)
        word_after = shortener.models.Word.query.get(key_after.offset)

        self.assertEqual(url_stored.id, shortener.controllers.hash_url(url_input))
        self.assertEqual(url_stored.url_full, url_input)
        self.assertEqual(url_stored.short, 't')

        self.assertEqual(key_after.id, 't')
        self.assertFalse(key_after.occupied)
        self.assertEqual(key_after.offset, 22825)

        self.assertEqual(word_updated.id, 22824)
        self.assertEqual(word_updated.slug, 't')
        self.assertEqual(word_updated.letter, 't')
        self.assertTrue(word_updated.occupied)

        self.assertEqual(word_after.id, 22825)
        self.assertEqual(word_after.slug, "t\'s")
        self.assertEqual(word_after.letter, 't')
        self.assertFalse(word_after.occupied)


    def test_url_02_repeated(self):
        url_input = 'http://techcrunch.com:5000/test/'

        # Mutation does not affect retrieved values
        url_before = shortener.models.Url.query.get(shortener.controllers.hash_url(url_input))
        key_before = shortener.models.Key.query.get('t')
        word_before = shortener.models.Word.query.get(key_before.offset)

        self.assertIsNotNone(url_before)
        self.assertEqual(url_before.id, shortener.controllers.hash_url(url_input))
        self.assertEqual(url_before.url_full, 'http://localhost:5000/test/')
        self.assertEqual(url_before.short, 't')

        self.assertEqual(key_before.id, 't')
        self.assertFalse(key_before.occupied)
        self.assertEqual(key_before.offset, 22825)

        self.assertEqual(word_before.id, 22825)
        self.assertEqual(word_before.slug, "t\'s")
        self.assertEqual(word_before.letter, 't')
        self.assertFalse(word_before.occupied)

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            ),
            follow_redirects=True
        )

        self.assert200(response)
        self.assertContext(
            name='description',
            value='Input your url',
        )
        self.assertContext(
            name='url_message',
            value=url_input
        )
        self.assertContext(
            name='short_url',
            value='http://domain.com/t'
        )
        self.assertContext(
            name='status_message',
            value='Url was found !'
        )

        url_after = shortener.models.Url.query.get(shortener.controllers.hash_url(url_input))
        key_after = shortener.models.Key.query.get('t')
        word_after = shortener.models.Word.query.get(key_before.offset)
        word_previous = shortener.models.Word.query.get(key_before.offset - 1)

        self.assertIsNotNone(url_after)
        self.assertEqual(url_after.id, shortener.controllers.hash_url(url_input))
        self.assertEqual(url_after.url_full, 'http://localhost:5000/test/')
        self.assertEqual(url_after.short, 't')

        self.assertEqual(key_after.id, 't')
        self.assertFalse(key_after.occupied)
        self.assertEqual(key_after.offset, 22825)

        self.assertEqual(word_after.id, 22825)
        self.assertEqual(word_after.slug, "t\'s")
        self.assertEqual(word_after.letter, 't')
        self.assertFalse(word_after.occupied)

        self.assertEqual(word_previous.id, 22824)
        self.assertEqual(word_previous.slug, 't')
        self.assertEqual(word_previous.letter, 't')
        self.assertTrue(word_previous.occupied)


    def test_url_03_altered(self):
        url_input = 'http://techcrunch.com:5000/test1/'

        url_before = shortener.models.Url.query.get(shortener.controllers.hash_url(url_input))
        key_before = shortener.models.Key.query.get('t')
        key_before_offset = key_before.offset
        word_before = shortener.models.Word.query.get(key_before.offset)

        self.assertIsNone(url_before)

        self.assertEqual(key_before.id, 't')
        self.assertFalse(key_before.occupied)
        self.assertEqual(key_before.offset, 22825)

        self.assertEqual(word_before.id, 22825)
        self.assertEqual(word_before.slug, "t\'s")
        self.assertEqual(word_before.letter, 't')
        self.assertFalse(word_before.occupied)

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            ),
            follow_redirects=True
        )

        self.assert200(response)
        self.assertContext(
            name='description',
            value='Input your url',
        )
        self.assertContext(
            name='url_message',
            value=url_input
        )
        self.assertContext(
            name='short_url',
            value='http://domain.com/t\'s'
        )
        self.assertContext(
            name='status_message',
            value='Url was not found. Stored in database.'
        )

        url_after = shortener.models.Url.query.get(shortener.controllers.hash_url(url_input))
        key_after = shortener.models.Key.query.get('t')
        word_after = shortener.models.Word.query.get(key_after.offset)
        word_updated = shortener.models.Word.query.get(key_before_offset)

        self.assertIsNotNone(url_after)
        self.assertEqual(url_after.id, shortener.controllers.hash_url(url_input))
        self.assertEqual(url_after.url_full, url_input)
        self.assertEqual(url_after.short, "t\'s")

        self.assertEqual(key_after.id, 't')
        self.assertFalse(key_after.occupied)
        self.assertEqual(key_after.offset, 22826)

        self.assertEqual(word_after.id, 22826)
        self.assertEqual(word_after.slug, 'tab')
        self.assertEqual(word_after.letter, 't')
        self.assertFalse(word_after.occupied)

        self.assertEqual(word_updated.id, 22825)
        self.assertEqual(word_updated.slug, "t\'s")
        self.assertEqual(word_updated.letter, 't')
        self.assertTrue(word_updated.occupied)


    def test_url_04__1_numeric(self):
        url_input = 'http://techcrunch.com:5000/1/'

        url_before = shortener.models.Url.query.get(shortener.controllers.hash_url(url_input))

        self.assertIsNone(url_before)

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            ),
            follow_redirects=True
        )

        self.assert200(response)
        self.assertContext(
            name='description',
            value='Input your url',
        )
        self.assertContext(
            name='url_message',
            value=url_input
        )
        self.assertContext(
            name='short_url',
            value='http://domain.com/10th'
        )
        self.assertContext(
            name='status_message',
            value='Url was not found. Stored in database.'
        )

        url_after = shortener.models.Url.query.get(shortener.controllers.hash_url(url_input))
        self.assertIsNotNone(url_after)
        self.assertEqual(url_after.id, shortener.controllers.hash_url(url_input))
        self.assertEqual(url_after.url_full, url_input)
        self.assertEqual(url_after.short, "10th")


    def test_url_05__11_numeric(self):
        url_input = 'http://techcrunch.com:5000/11/'

        url_before = shortener.models.Url.query.get(shortener.controllers.hash_url(url_input))

        self.assertIsNone(url_before)

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            ),
            follow_redirects=True
        )

        self.assert200(response)
        self.assertContext(
            name='description',
            value='Input your url',
        )
        self.assertContext(
            name='url_message',
            value=url_input
        )
        self.assertContext(
            name='short_url',
            value='http://domain.com/1st'
        )
        self.assertContext(
            name='status_message',
            value='Url was not found. Stored in database.'
        )

        url_after = shortener.models.Url.query.get(shortener.controllers.hash_url(url_input))
        self.assertIsNotNone(url_after)
        self.assertEqual(url_after.id, shortener.controllers.hash_url(url_input))
        self.assertEqual(url_after.url_full, url_input)
        self.assertEqual(url_after.short, "1st")


    def test_url_06__111_numeric(self):
        url_input = 'http://techcrunch.com:5000/111/'

        url_before = shortener.models.Url.query.get(shortener.controllers.hash_url(url_input))
        key_before = shortener.models.Key.query.get('1')
        key_before_offset = key_before.offset
        word_before = shortener.models.Word.query.get(key_before_offset)

        self.assertIsNone(url_before)
        self.assertEqual(key_before.id, '1')
        self.assertTrue(key_before.occupied)
        self.assertEqual(key_before.offset, 2)

        self.assertEqual(word_before.id, 2)
        self.assertEqual(word_before.slug, "1st")
        self.assertEqual(word_before.letter, '1')
        self.assertTrue(word_before.occupied)

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            ),
            follow_redirects=True
        )

        self.assert200(response)
        self.assertMessageFlashed('Ooops! Not generated url. The database does not contain any more slugs for this type of url path.', category='error')

        url_after = shortener.models.Url.query.get(shortener.controllers.hash_url(url_input))
        key_after = shortener.models.Key.query.get('1')
        key_after_offset = key_after.offset
        word_after = shortener.models.Word.query.get(key_after_offset)

        self.assertIsNone(url_after)
        self.assertEqual(key_after.id, '1')
        self.assertTrue(key_after.occupied)
        self.assertEqual(key_after.offset, 2)

        self.assertEqual(word_after.id, 2)
        self.assertEqual(word_after.slug, "1st")
        self.assertEqual(word_after.letter, '1')
        self.assertTrue(word_after.occupied)


    def test_url_07__1112_numeric(self):
        url_input = 'http://techcrunch.com:5000/1112/'

        url_before = shortener.models.Url.query.get(shortener.controllers.hash_url(url_input))
        key_before = shortener.models.Key.query.get('2')
        key_before_offset = key_before.offset
        word_before = shortener.models.Word.query.get(key_before_offset)

        self.assertIsNone(url_before)
        self.assertEqual(key_before.id, '2')
        self.assertFalse(key_before.occupied)
        self.assertEqual(key_before.offset, 3)

        self.assertEqual(word_before.id, 3)
        self.assertEqual(word_before.slug, "2nd")
        self.assertEqual(word_before.letter, '2')
        self.assertFalse(word_before.occupied)

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            ),
            follow_redirects=True
        )

        self.assert200(response)
        self.assertContext(
            name='description',
            value='Input your url',
        )
        self.assertContext(
            name='url_message',
            value=url_input
        )
        self.assertContext(
            name='short_url',
            value='http://domain.com/2nd'
        )
        self.assertContext(
            name='status_message',
            value='Url was not found. Stored in database.'
        )

        url_after = shortener.models.Url.query.get(shortener.controllers.hash_url(url_input))
        key_after = shortener.models.Key.query.get('2')
        key_after_offset = key_after.offset
        word_after = shortener.models.Word.query.get(key_after_offset)

        self.assertIsNotNone(url_after)
        self.assertEqual(url_after.id, shortener.controllers.hash_url(url_input))
        self.assertEqual(url_after.url_full, url_input)
        self.assertEqual(url_after.short, "2nd")

        self.assertEqual(key_after.id, '2')
        self.assertTrue(key_after.occupied)
        self.assertEqual(key_after.offset, 3)

        self.assertEqual(word_after.id, 3)
        self.assertEqual(word_after.slug, "2nd")
        self.assertEqual(word_after.letter, '2')
        self.assertTrue(word_after.occupied)


    def test_url_08_full_database(self):
        url_input = 'http://techcrunch.com:5000/example/'

        # Mock fill database
        result = shortener.controllers.test_full_db()
        self.assertEqual(result, 'Key table was marked occupied.')


        keys_after = shortener.models.Key.query.all()
        words_after = shortener.models.Word.query.all()

        for key_after in keys_after:
            self.assertTrue(key_after.occupied)

        response = self.client.post(
            url_for('add'),
            data = dict(
                url=url_input
            )
        )

        self.assert200(response)
        self.assertMessageFlashed('Ooops! Not generated url. The database is full. Cannot store {}'.format(url_input), category='error')

