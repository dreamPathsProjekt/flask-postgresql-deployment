from flask import render_template, url_for, request, redirect, flash
from validators import url
from shortener.helpers import custom_validation
from shortener import app, controllers
from shortener.models import Url, Word


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', description='Input your url')


@app.route('/add', methods=['POST', 'GET'])
def add():
    if request.method == 'POST':
        url_input = request.form['url']

        if not validate_url(url_input):
            return render_template('index.html', description='Input your url')

        result = controllers.retrieve_or_store(url_input)

        if not check_for_db_errors(result):
            return render_template('index.html', description='Input your url')
        if not check_for_full_db(result):
            return render_template('index.html', description='Input your url')

        return render_template(
            'index.html',
            description='Input your url',
            url_message=url_input,
            short_url='http://domain.com/{}'.format(result.short),
            status_message=result.status
        )

    return render_template('index.html', description='Input your url')


@app.route('/initdb')
def initdb():
    return controllers.create_schema()


@app.route('/dropdb')
def dropdb():
    return controllers.drop_schema()


@app.route('/initdata')
def init_data():
    return controllers.initial_data()


@app.route('/reset')
def rollback_schema():
    return controllers.rollback_db()


@app.route('/full')
def test_full():
    return controllers.test_full_db()


def validate_url(url_input):
    if not url(url_input):
        flash('The requested url {} was not valid.'.format(url_input), category='error')
        return False

    if custom_validation(url_input) != 'Passed':
        flash('{}'.format(custom_validation(url_input)), category='error')
        return False

    return True


def check_for_db_errors(result):
    if result is None:
        flash('An error occured while searching and/or storing the result,  {}. Please try again.'.format(result.url), category='error')
        return False
    return True


def check_for_full_db(result):
    error_message_full_db = 'The database is full. Cannot store {}'.format(result.url)
    error_message_this_charset = 'The database does not contain any more slugs for this type of url path.'
    if result.status == error_message_full_db or result.status == error_message_this_charset:
        flash('{} {}'.format(result.short, result.status), category='error')
        return False
    return True


