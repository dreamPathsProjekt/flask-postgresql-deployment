from shortener import db
import hashlib
from shortener.helpers import hash_url


class Url(db.Model):
    id = db.Column(db.String(120), primary_key=True, unique=True)
    url_full = db.Column(db.String(200), unique=True)
    short = db.Column(db.String(200))


class Word(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    slug = db.Column(db.String(150), unique=True)
    letter = db.Column(db.String(10))
    occupied = db.Column(db.Boolean, default=False)


class Key(db.Model):
    id = db.Column(db.String(10), primary_key=True, unique=True)
    offset = db.Column(db.Integer, unique=True)
    occupied = db.Column(db.Boolean, default=False)