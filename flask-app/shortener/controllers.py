from shortener import db, app, models
from shortener.helpers import hash_url, parse_url
from collections import namedtuple
from sqlalchemy import exc


Answer = namedtuple('Answer', ['url', 'short', 'status'])


def retrieve_or_store(url_input):

    result = check_url(url_input)
    if result is not None:
        return Answer(
            url=url_input,
            short=result.short,
            status='Url was found !'
        )

    return store_url(url_input)


def check_url(url_input):
    parsed = parse_url(url_input)
    hashed_input = hash_url(parsed.full_path)
    return models.Url.query.get(hashed_input)


def store_url(url_input):

    error_message_full = 'The database is full. Cannot store {}'.format(url_input)
    error_message_series = 'The database does not contain any more slugs for this type of url path.'
    result = generate_slug(url_input)

    if result == error_message_full or result == error_message_series:
        return Answer(
            url=url_input,
            short='Ooops! Not generated url.',
            status='{}'.format(result)
        )


    if result is not None:
        url_instance = models.Url(
            id=hash_url(url_input),
            url_full=url_input,
            short=result
        )

        db.session.add(url_instance)
        db.session.commit()

        return Answer(
            url=url_input,
            short=url_instance.short,
            status='Url was not found. Stored in database.'
        )

    return None


def generate_slug(url_input):
    parsed = parse_url(url_input)

    char_keys_not_occupied = models.Key.query.filter_by(occupied=False).all()
    # Check if all the keys are occupied, query returns empty list, not None.
    if  char_keys_not_occupied == []:
        return 'The database is full. Cannot store {}'.format(url_input)


    # Check if all keys are occupied for the given character set.
    keys_ids = list(map(lambda key: key.id, char_keys_not_occupied))
    if not any(letter in keys_ids for letter in parsed.character_set):
        return 'The database does not contain any more slugs for this type of url path.'


    for character in parsed.character_set:
        char_key = get_key(character)
        # if char_key.occupied then get_key() returns None. Move to the next character.
        if char_key is not None:
            word = get_word(char_key.offset)
            if word is not None and word.letter == char_key.id:
                commit_offset(char_key, word)
                return word.slug

    return None


def get_word(offset):
    word = models.Word.query.get(offset)
    if not word.occupied:
        return word
    return None


def get_key(character):
    char_key = models.Key.query.get(character)
    if char_key is not None and not char_key.occupied:
        return char_key
    return None


def commit_offset(char_key, word):
    # update word occupied
    word.occupied = True
    next_word = models.Word.query.get(word.id + 1)

    if next_word.letter == char_key.id:
        # update key offset
        char_key.offset = word.id + 1
    else:
        # update key occupied, reached the end of the wordlist for this letter.
        char_key.occupied = True

    db.session.commit()


def create_schema():
    db.create_all()
    return 'Database Schema Created'


def drop_schema():
    db.drop_all()
    return 'Database Schema Dropped'


def initial_data():
    try:
        with open('wordlist.txt', mode='r') as wordlist:
            lines = wordlist.readlines()
            lines_sanitized = [word.strip() for word in lines]
            keys = set(list(word)[0] for word in lines_sanitized)

            for key_id in keys:
                key_instance = models.Key(
                    id=key_id
                )
                db.session.add(key_instance)


            for word in sorted(lines_sanitized):
                word_instance = models.Word(
                    slug=word,
                    letter=list(word)[0]
                )
                db.session.add(word_instance)

            db.session.flush()

            # Offsets are used in keys/letters as a way to fast search the first non-occupied word.
            # Searching is faster if the id of words is indexed.
            # They cannot be used as foreign keys to the ids of words, because they are updated during inserts.
            for key_id in keys:
                words_match = models.Word.query.filter_by(letter=key_id).first()
                key_match = models.Key.query.get(key_id)
                key_match.offset = words_match.id

            db.session.flush()

        db.session.commit()

        return 'Wordlist populated & stored on database'

    except exc.DatabaseError:
        return rollback_db()


# If for any case an error occurs after the first commit of keys and words,
# rollback procedure by droping and re-creating schema.
# Initial data has to be called again manually
def rollback_db():
    drop_schema()
    create_schema()
    return 'Schema was dropped and recreated. Please run /initdata again.'


def test_full_db():
    for char_key in models.Key.query.all():
        char_key.occupied = True
    db.session.commit()
    return 'Key table was marked occupied.'
