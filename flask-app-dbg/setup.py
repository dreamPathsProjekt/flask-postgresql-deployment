from setuptools import setup, find_packages

setup(
    name="shortener",
    version="0.8",
    # packages=find_packages(),


    install_requires=[
        "flask==1.0.1",
        "flask-sqlalchemy",
        "psycopg2",
        "validators",
        "flask-testing",
        "nose",
        "blinker",
        "flask-wdb"
        # "requests"
    ],
    packages=['shortener', 'tests'],
    # metadata for upload to PyPI
    author="Ioannis Dritsas",
    author_email="dream.paths.projekt@gmail.com",
    description="Flask URL shortener with Postgresql",
    license="MIT",
    keywords="",
    url="https://gitlab.com/dreamPathsProjekt/flask-postgresql-deployment",
)
