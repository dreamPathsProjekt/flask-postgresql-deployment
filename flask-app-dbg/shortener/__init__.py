from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
import uuid


# Initial configs
user = os.environ['POSTGRES_USER']
password = os.environ['POSTGRES_PASSWORD']
database = os.environ['POSTGRES_DB']
host = os.environ['POSTGRES_HOST']
port = os.environ['POSTGRES_PORT']
session_key = os.urandom(128)


app = Flask(__name__)
# Redirect with or without slashes
app.url_map.strict_slashes = False
# dialect+driver://username:password@host:port/database
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres://{}:{}@{}:{}/{}'.format(user, password, host, port, database)
app.config['SECRET_KEY'] = session_key
db = SQLAlchemy(app)


# Avoid cyclic imports
from . import models
from . import controllers
from . import views
