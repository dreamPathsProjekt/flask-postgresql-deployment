import hashlib, re, os
from urllib.parse import urlparse
from posixpath import basename, dirname
from collections import namedtuple, OrderedDict


def hash_url(url_input):
    # Hasher hashes only the dirname and basepath and not the hostname, based on implementation requirements.
    parsed = parse_url(url_input)
    return hashlib.sha256(parsed.full_path.encode()).hexdigest()


def parse_url(url_input):

    ParsedUrl = namedtuple(
            'ParsedUrl',
            ['full_path', 'path', 'base', 'character_list', 'character_set']
        )

    regex = re.compile('[^a-zA-Z0-9]')

    parse_object = urlparse(url_input)
    path = regex.sub('', dirname(parse_object.path))
    base = regex.sub('', basename(parse_object.path))

    character_list = list(path + base)

    items = character_list
    character_set = list(OrderedDict.fromkeys(items))


    return ParsedUrl(
        full_path=parse_object.path,
        path=path,
        base=base,
        character_list=character_list,
        character_set=character_set
    )


def custom_validation(url_input):
    parsed = parse_url(url_input)
    regex = re.compile('[^a-zA-Z0-9]')

    if parsed.character_set == ['0']:
        return 'This implementation does not allow zeroes only as url path.'


    if parsed.character_set == []:

        if regex.match(parsed.full_path):
            return 'Not allowed only special characters as url path.'

        return 'Empty url path is not permitted !'

    return 'Passed'
