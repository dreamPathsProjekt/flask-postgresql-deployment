from shortener import app, db, models, views, controllers
from flask_wdb import Wdb

if __name__ == "__main__":
    Wdb(app)
    app.run(
        debug=True,
        host='0.0.0.0',
        port=5000,
        use_evalex=False,
        threaded=True
    )